import argparse
import pathlib
import string
import sys
import hashlib
import hmac

AVAILABLE_MASKS = ['a', 'd', 'l', 'u']
CONTENT_CHUNKS = 8
PFX_PREFIX = 'pfxng'


def get_mac_algo(id):
    if id == 1:
        return "sha1"
    elif id == 224:
        return "sha224"
    elif id == 256:
        return "sha256"
    elif id == 384:
        return "sha384"
    elif id == 512:
        return "sha512"
    else:
        sys.stderr.write("mac_id %s is not supported yet!\n" % id)
        return


def alphabet(letter):
    if letter == 'a':
        return list(string.ascii_letters) + list(string.digits)
    if letter == 'l':
        return list(string.ascii_lowercase)
    if letter == 'u':
        return list(string.ascii_uppercase)
    if letter == 'd':
        return list(string.digits)


def check_mask_template(mask):
    result = True
    for mask_parameter in mask:
        result &= mask_parameter in AVAILABLE_MASKS
    return result


def passwords_candidates(mask, dict_file):
    alphabets = [alphabet(letter) for letter in mask]
    with open(dict_file, "r") as f:
        res = f.readlines()
    words = []
    for word in res:
        if word[-1] == '\n':
            words.append(word[:-1])
        else:
            words.append(word)
    return [words] + alphabets


# MAC_ALGO $ KEY_LENGTH (MAC) $ ITERATIONS $ SIZE (SALT) $ SALT $ DATA $ HMAC
def rebuild(file):
    data = {}
    with open(file, "r") as f:
        content = f.read().split('$')
        if (len(content) == CONTENT_CHUNKS) & (content[0] == PFX_PREFIX):
            data['mac'] = get_mac_algo(int(content[1]))
            data['key_length'] = int(content[2])
            data['iterations'] = int(content[3])
            data['size'] = int(content[4])
            data['salt'] = bytes.fromhex(content[5])
            data['data'] = bytes.fromhex(content[6])
            data['hmac'] = bytes.fromhex(content[7])

            return data


def ords(alphabets):
    passwords_count = 1
    ords_bases = []

    for custom_alphabet in alphabets:
        passwords_count *= len(custom_alphabet)

    for i in range(len(alphabets)):
        if i == 0:
            ords_bases.append(len(alphabets[i]))
        else:
            ords_bases.append(ords_bases[i-1] * len(alphabets[i]))

    return ords_bases, passwords_count


def pbkdf2(password, salt, algo, iteration, exp_key_len):
    return hashlib.pbkdf2_hmac(
    algo,
    password,
    salt,
    iteration,
    dklen=exp_key_len)


def prf(hash_type, password, data):
    return hmac.new(password, data, hash_type).digest()


def brute_force(passwords_alph, content):
    bases, passwords = ords(passwords_alph)

    for password in range(passwords):
        password_word = ''
        for i in range(len(bases)):
            mask_alphabet = passwords_alph[i]
            len_alphabet = len(mask_alphabet)
            if i == 0:
                password_word += mask_alphabet[password % bases[i]]
            else:
                password_word += mask_alphabet[((password * len_alphabet) // bases[i]) % len_alphabet]

        key = pbkdf2(password_word.encode(),
                     content['salt'],
                     content['mac'],
                     content['iterations'],
                     content['key_length'])
        hash = prf(content['mac'], key, content['data']).hex()

        if content['hmac'].hex() == hash:
            print('Password recovered! Password : ', password_word)
            return
    print('The password was not found during the enumeration.')


def crack_pkcs12(args):
    if check_mask_template(args.mask):
        passwords = passwords_candidates(args.mask, args.dict)
        content = rebuild(args.source_file)
        brute_force(passwords, content)
    else:
        sys.stderr.write("such type of mask is not supported yet!\n")
        return


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.version = '1.0'
    parser.add_argument('-v',
                        '--version',
                        action='version')
    parser.add_argument('-d',
                        '--dict',
                        type=pathlib.Path,
                        action='store',
                        help='file with dictionary enumeration. it has specified format')
    parser.add_argument('-m',
                        '--mask',
                        action='store',
                        type=str,
                        required=True,
                        help=
                        '''set the mask for password enumeration 
                            a – all latin letters (u + l) + digits; 
                            d – only digits; 
                            l – lower latin letters; 
                            u – upper latin letters. 
                        ''')
    parser.add_argument('source_file',
                        type=pathlib.Path,
                        action='store',
                        help='file with encrypted information. it has specified content format')

    crack_pkcs12(parser.parse_args())