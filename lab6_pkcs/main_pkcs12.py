#!/usr/bin/env python
# coding: utf-8

import binascii
import sys
try:
    from asn1crypto import pkcs12
except ImportError:
    sys.stderr.write("asn1crypto is missing, run 'pip install --user asn1crypto' to install it!\n")
    sys.exit(1)


def parse_pkcs12(filename):
    data = open(filename, "rb").read()
    pfx = pkcs12.Pfx.load(data)

    auth_safe = pfx['auth_safe']
    if auth_safe['content_type'].native != 'data':
        raise ValueError(
            '''
            Only password-protected PKCS12 files are currently supported
            '''
        )

    mac_data = pfx['mac_data']
    if mac_data:
        mac_algo = mac_data['mac']['digest_algorithm']['algorithm'].native
        key_length = {
            'sha1': 20,
            'sha224': 28,
            'sha256': 32,
            'sha384': 48,
            'sha512': 64,
            'sha512_224': 28,
            'sha512_256': 32,
        }[mac_algo]

        salt = mac_data['mac_salt'].native
        iterations = mac_data['iterations'].native
        if mac_algo == "sha1":
            mac_algo_numeric = 1
        elif mac_algo == "sha224":
            mac_algo_numeric = 224
        elif mac_algo == "sha256":
            mac_algo_numeric = 256
        elif mac_algo == "sha384":
            mac_algo_numeric = 384
        elif mac_algo == "sha512":
            mac_algo_numeric = 512
        else:
            sys.stderr.write("mac_algo %s is not supported yet!\n" % mac_algo)
            return
        stored_hmac = mac_data['mac']['digest'].native
        data = auth_safe['content'].contents
        size = len(salt)
        filename = "%s_%s_%s.txt" % (mac_algo, str(iterations), str(key_length))
        with open(filename, "w") as file:
            file.write("pfxng$%s$%s$%s$%s$%s$%s$%s\n" %
                             (mac_algo_numeric,
                              key_length,
                              iterations,
                              size,
                              binascii.hexlify(salt).decode(),
                              binascii.hexlify(data).decode(),
                              binascii.hexlify(stored_hmac).decode()))


# MAC_ALGO $ KEY_LENGTH (MAC) $ ITERATIONS $ SIZE (SALT) $ SALT $ DATA $ HMAC
if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.stderr.write("Usage: %s <.pfx file(s)>\n" % sys.argv[0])

    for i in range(1, len(sys.argv)):
        parse_pkcs12(sys.argv[i])
