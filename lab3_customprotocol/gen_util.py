import argparse
import cipher
import hmacgen
import prob_random
import fbuilder

DEFAULT_PLAIN_TEXT = 'Hello from the crypto app!'

BYTES_LOWER = {'0': 0x00, '1': 0x01, '2': 0x02, '3': 0x03, '4': 0x04, '5': 0x05, '6': 0x06, '7': 0x07, '8': 0x08, '9': 0x09,
         'a': 0x0a, 'b': 0x0b, 'c': 0x0c, 'd': 0x0d, 'e': 0x0e, 'f': 0x0f,
         'A': 0x0a, 'B': 0x0b, 'C': 0x0c, 'D': 0x0d, 'E': 0x0e, 'F': 0x0f}

BYTES_HIGHER = {'0': 0x00, '1': 0x10, '2': 0x20, '3': 0x30, '4': 0x40, '5': 0x50, '6': 0x60, '7': 0x70, '8': 0x80, '9': 0x90,
         'a': 0xa0, 'b': 0xb0, 'c': 0xc0, 'd': 0xd0, 'e': 0xe0, 'f': 0xf0,
         'A': 0xa0, 'B': 0xb0, 'C': 0xc0, 'D': 0xd0, 'E': 0xe0, 'F': 0xf0}

KEY_SIZES = {'3des': 16, 'aes-128': 16, 'aes-192': 24, 'aes-256': 32}


def check_lens(password):
    if len(password) != 8:
        raise Exception('Incorrect password parameter length!')


def create_key(password, hmac_type, key_size):
    nonce = prob_random.gen_nonce()
    return (hmacgen.create_hmac(
        key_size,
        hmacgen.HashType.SHA1 if hmac_type == 'sha1' else hmacgen.HashType.MD5,
        password,
        nonce), nonce)


def create_key_nonce(password, hmac_type, key_size, nonce):
    return hmacgen.create_hmac(
        key_size,
        hmacgen.HashType.SHA1 if hmac_type == 'sha1' else hmacgen.HashType.MD5,
        password,
        nonce)


def get_bytes_from_str(string):
    res = []
    for i in range(0, 8, 2):
        if i % 2 == 0:
            res.append(BYTES_HIGHER[string[i]] + BYTES_LOWER[string[i+1]])

    return bytes(res)


def process(args):
    check_lens(args.passw)
    passw = get_bytes_from_str(args.passw)
    key, nonce = create_key(passw, args.hash, KEY_SIZES[args.algo])

    encryptor = cipher.DES3Cipher(key) if args.algo == '3des' else cipher.AESCipher(key)
    cipher_text = encryptor.encrypt(DEFAULT_PLAIN_TEXT)
    fbuilder.build_file(cipher_text,
                        args.algo,
                        args.hash,
                        args.passw,
                        encryptor.iv,
                        nonce)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.version = '1.0'
    parser.add_argument('-v',
                        '--version',
                        action='version')
    parser.add_argument('-p', '--passw',
                        action='store',
                        type=str,
                        required=True,
                        help='set the password (only 4 bytes)')
    parser.add_argument('-hs',
                        '--hash',
                        action='store',
                        type=str,
                        required=True,
                        help='set the hash encryption/decryption mode ',
                        choices=['md5', 'sha1'])
    parser.add_argument('-a',
                        '--algo',
                        action='store',
                        required=True,
                        type=str,
                        help='set the algo encryption/decryption mode ',
                        choices=['3des', 'aes-128', 'aes-192', 'aes-256'])

    process(parser.parse_args())
