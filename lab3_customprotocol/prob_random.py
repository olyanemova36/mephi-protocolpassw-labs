import secrets
import string

NONCE_SIZE = 64


def gen_nonce():
    result = ''.join(secrets.choice(string.ascii_uppercase + string.ascii_lowercase) for i in range(NONCE_SIZE))
    return result.encode()


def gen_iv(size):
    result = ''.join(secrets.choice(string.ascii_uppercase + string.ascii_lowercase) for i in range(size))
    return result.encode()
