# Custom Protocol

Перед работой c утилитами для импортирования алгоритмов шифрования, следует установить следующие пакеты:

```shell
pip install pycryptodome  

sudo pip install pycrypto
sudo pip install crypto
```
## How to understand the utility output: 

Весь протокол состоит из 3-х утилит `gen_util.py, verify_util.py, crack_util.py`. Подробное описание использования всех утилит смотрите далее.

### How to use gen_util.py

Данная утилита генерирует файл по шаблону `[HASH]_[ALGO]_[PASSWORD].enc`. Содержимое файла это зашифрованный текст 'Hello from the crypto app!' 
взятый как константа для генерации всех возможных файлов. Запуск утилиты и справка для её аргументов: 

```shell
(venv) ➜ custom-protocol git:(main) python gen_util.py -h   
```

```shell
usage: gen_util.py [-h] [-v] -p PASSW -hs {md5,sha1} -a {3des,aes-128,aes-192,aes-256}

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program_s version number and exit
  -p PASSW, --passw PASSW
                        set the password (only 4 bytes)
  -hs {md5,sha1}, --hash {md5,sha1}
                        set the hash encryption/decryption mode
  -a {3des,aes-128,aes-192,aes-256}, --algo {3des,aes-128,aes-192,aes-256}
                        set the algo encryption/decryption mode
```

Пример работы утилиты. Возьмем несколько примеров вызовов и посмотрим их 
соответствущие выводы.  

```shell
(venv) ➜ custom-protocol git:(main) python gen_util.py -p 0000111f -hs md5 -a 3des
(venv) ➜ custom-protocol git:(main) python gen_util.py -p 0000002f -hs md5 -a aes-128
```

В рутовой директории создадутся соответственно файлы с дескрипторами, 
содержащие переданные параметры. 

### How to use verify_util.py

Данная утилита проверяет переданный файл на наличие в нем шифр текста и правильный формат внутренних данных.
Запуск утилиты и справка для её аргументов: 

```shell
(venv) ➜ custom-protocol git:(main) python verify_util.py -h   
```

```shell         
usage: verify_util.py [-h] [-v] source_file

positional arguments:
  source_file    file with encrypted text

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version number and exit
```
Пример работы утилиты. Возьмем несколько примеров вызовов и посмотрим их 
соответствущие выводы.  

```shell
(venv) ➜ custom-protocol git:(main) python verify_util.py md5_aes-128_0000abcd.enc 
(venv) ➜ custom-protocol git:(main) python verify_util.py somefile.txt 
```
Соответствующие выводы утилиты для вводов выше. 
```shell
True
-------
False
```

### How to use crack_util.py

Данная утилита находит пароль для шифртекста полным перебором всех возможных паролей до 
первого удачного пароля. Также утилита по запросу пользователя выводит логи процесса перебора 
и текущую скорость для просмотренного пароля. В конце утилита выводит найденный пароль и конечную скорость поиска. 
Запуск утилиты и справка для её аргументов:

```shell
(venv) ➜  custom-protocol git:(main) ✗ python crack_util.py -h 
```

```shell         
usage: crack_util.py [-h] [-d] source_file

positional arguments:
  source_file  file with encrypted text

optional arguments:
  -h, --help   show this help message and exit
  -d, --debug  run utility in debug mode (with logging)
```

Пример работы утилиты. Возьмем несколько примеров вызовов и посмотрим их соответствущие выводы.

```shell
(venv) ➜ custom-protocol git:(main) python crack_util.py sha1_3des_0000111f.enc
(venv) ➜ custom-protocol git:(main) python crack_util.py -d sha1_3des_0000111f.enc
```
Соответствующие выводы утилиты в консоль с предустановленным флагом вывода трейсов и нет для вводов выше.
```shell
HMAC:  sha1   3des
NONCE:  4a454c4a4c565553566e746d47454772766d6a61416c7965775347484f495953784e4c587976476b504b7662726e6458446e614e6d4f526472766b534e775458
IV:  6351456a63686d4e
CT:  59314646616d4e6f6255344f4d5862762b717771344538516b496a654236662f44643837756d2f653451412b634d594d386b6c4139773d3d
Start cracking...
MESSAGE :  Hello from the crypto app!
Found password! 0000111f  | Speed:  10809  c/s...
--------------------
HMAC:  sha1   3des
NONCE:  4a454c4a4c565553566e746d47454772766d6a61416c7965775347484f495953784e4c587976476b504b7662726e6458446e614e6d4f526472766b534e775458
IV:  6351456a63686d4e
CT:  59314646616d4e6f6255344f4d5862762b717771344538516b496a654236662f44643837756d2f653451412b634d594d386b6c4139773d3d
Start cracking...
Current:  00000000  -  00000020  | Speed:  0  c/s...
Current:  00001000  -  00001020  | Speed:  10764  c/s...
MESSAGE :  Hello from the crypto app!
Found password! 0000111f  | Speed:  10729  c/s..
```
