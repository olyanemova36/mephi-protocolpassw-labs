import argparse
import pathlib
import fbuilder
import cipher
import gen_util
import time
import math

KEY_SIZES = {'3des': 16, 'aes-128': 16, 'aes-192': 24, 'aes-256': 32}
CIPHER_ID = {'0': '3des', '1': 'aes-128', '2': 'aes-192', '3': 'aes-256'}
HMAC_ID = {'0': 'md5', '1': 'sha1'}

DEFAULT_PLAIN_TEXT = 'Hello from the crypto app!'
STEP = 64 * 64

def brute_force(before, incr):
    plaintext_length = len(before)
    plaintext_number = int.from_bytes(before, 'big')

    plaintext_number += incr
    nextable = plaintext_number.to_bytes(plaintext_length, 'big')
    return nextable


def checked(hmac_algo_possib, hmac, encrypt_algo_possib, algo):
    return (hmac == HMAC_ID[hmac_algo_possib]) & (algo == CIPHER_ID[encrypt_algo_possib])


def process(args):
    hmac_algo_possib, encrypt_algo_possib, nonce_possib, iv_possib, cipher_text_poss, passw = fbuilder.rebuild_file_except(
        args.source_file)

    parameters = str(args.source_file).split('_')
    hmac = parameters[0]
    algo = parameters[1]

    if checked(hmac_algo_possib, hmac, encrypt_algo_possib, algo):
        print("HMAC: ", HMAC_ID[hmac_algo_possib], " ", CIPHER_ID[encrypt_algo_possib])
        print("NONCE: ", nonce_possib)
        print("IV: ", iv_possib)
        print("CT: ", cipher_text_poss)

        print("Start cracking...")
        cipher_text = bytes()
        password_guess = bytes([0x00, 0x00, 0x00, 0x00])
        iv = bytes.fromhex(iv_possib)
        nonce = bytes.fromhex(nonce_possib)
        ciphertext = bytes.fromhex(cipher_text_poss).decode()

        counter = 0

        begin = time.time()
        message = []
        while ciphertext != cipher_text.decode():
            key = gen_util.create_key_nonce(password_guess, hmac, KEY_SIZES[algo], nonce)

            if (counter % STEP == 0) & (args.debug):
                current = time.time()
                print("Current: ", password_guess.hex(), " - ", brute_force(password_guess, 32).hex(), " | Speed: ",
                      math.ceil(counter/(current - begin)), " c/s...")
            encryptor = cipher.DES3Cipher(key) if algo == '3des' else cipher.AESCipher(key)
            cipher_text = encryptor.encrypt_iv(DEFAULT_PLAIN_TEXT, iv)
            message = encryptor.decrypt(cipher_text).decode()
            password_guess = brute_force(password_guess, 1)
            counter += 1

        if ciphertext == cipher_text.decode():
            end = time.time()
            print("MESSAGE : ", message)
            print("Found password!", brute_force(password_guess, -1).hex(), " | Speed: ",
                    math.ceil(counter/(end - begin)), " c/s...")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-d', '--debug',
                        action='store_true',
                        help='run utility in debug mode (with logging)')
    parser.add_argument('source_file',
                        type=pathlib.Path,
                        action='store',
                        help='file with encrypted text')

    process(parser.parse_args())
