def get_id(algo):
    if algo == '3des':
        return '0'
    elif algo == 'aes-128':
        return '1'
    elif algo == 'aes-192':
        return '2'
    else:
        return '3'


def iv_size(id):
    return 8 if id == '0' else 16


def build_file(enc_text, algo, hmac, passw, iv, nonce):
    with open(str(hmac) + '_' + str(algo) + '_' + str(passw) + '.enc', 'w') as f:
        f.write('ENC')
        f.write('0' if hmac == 'md5' else '1')
        f.write(get_id(algo))
        print("NONCE : ", nonce.hex())
        f.write(nonce.hex())
        print("IV : ", iv.hex())
        f.write(iv.hex())
        print("CIPHER TEXT : ", enc_text.hex())
        f.write(enc_text.hex())


def rebuild_file(file_name):
    parameters = str(file_name).split('_')
    password = parameters[2][0:4]

    with open(file_name, "r") as f:

        try:
            content = f.read()
            enc_message = content[0:3]
            hmac_algo_possib = content[3]
            encrypt_algo_possib = content[4]
            nonce_possib = bytes.fromhex(content[5:133])
            iv_len = iv_size(encrypt_algo_possib)
            iv_end = 133 + (iv_len * 2)
            iv_possib = bytes.fromhex(content[133:iv_end])
            cipher_text = bytes.fromhex(content[iv_end:])
        except Exception:
            print(False)
            exit(0)

        if (enc_message != 'ENC') | (len(cipher_text) > 4096):
            print(False)
            exit(0)

        return hmac_algo_possib, encrypt_algo_possib, nonce_possib, iv_possib, cipher_text, password


def rebuild_file_except(file_name):
    parameters = str(file_name).split('_')
    password = parameters[2][0:4]

    with open(file_name, "r") as f:

        content = f.read()
        enc_message = content[0:3]
        hmac_algo_possib = content[3]
        encrypt_algo_possib = content[4]
        nonce_possib = content[5:133]
        iv_len = iv_size(encrypt_algo_possib)
        iv_end = 133 + (iv_len * 2)
        iv_possib = content[133:iv_end]
        cipher_text = content[iv_end:]

        if (enc_message != 'ENC') | (len(cipher_text) > 4096):
            raise ValueError("Incorrect file for input")

        return hmac_algo_possib, encrypt_algo_possib, nonce_possib, iv_possib, cipher_text, password


