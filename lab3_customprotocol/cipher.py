from base64 import b64decode
from base64 import b64encode

from Crypto.Cipher import AES
from Crypto.Cipher import DES3
from Crypto.Util.Padding import pad, unpad

import prob_random

AES_128_SIZE = 128
AES_192_SIZE = 192
AES_256_SIZE = 256
MAX_DATA_SIZE = 4096


class AESCipher:
    def __init__(self, key):
        self.key = key

    def encrypt(self, data):

        if len(data) > MAX_DATA_SIZE:
            raise ValueError("Incorrect data size, current - {0}, max - {}", len(data), MAX_DATA_SIZE)
        self.iv = prob_random.gen_iv(AES.block_size)
        self.cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        return b64encode(self.iv + self.cipher.encrypt(pad(data.encode('utf-8'),
                                                      AES.block_size)))

    def encrypt_iv(self, data, iv):

        if len(data) > MAX_DATA_SIZE:
            raise ValueError("Incorrect data size, current - {0}, max - {}", len(data), MAX_DATA_SIZE)
        self.iv = iv
        self.cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        return b64encode(self.iv + self.cipher.encrypt(pad(data.encode('utf-8'),
                                                      AES.block_size)))

    def decrypt(self, data):
        raw = b64decode(data)
        self.cipher = AES.new(self.key, AES.MODE_CBC, raw[:AES.block_size])
        return unpad(self.cipher.decrypt(raw[AES.block_size:]), AES.block_size)


class DES3Cipher:
    def __init__(self, key):
        while True:
            try:
                self.key = DES3.adjust_key_parity(key)
                break
            except ValueError:
                pass

    def encrypt(self, data):
        if len(data) > MAX_DATA_SIZE:
            raise ValueError("Incorrect data size, current - {0}, max - {}", len(data), MAX_DATA_SIZE)
        self.iv = prob_random.gen_iv(DES3.block_size)
        self.cipher = DES3.new(self.key, DES3.MODE_CBC, self.iv)
        return b64encode(self.iv + self.cipher.encrypt(pad(data.encode('utf-8'),
                                                      DES3.block_size)))

    def encrypt_iv(self, data, iv):
        if len(data) > MAX_DATA_SIZE:
            raise ValueError("Incorrect data size, current - {0}, max - {}", len(data), MAX_DATA_SIZE)
        self.iv = iv
        self.cipher = DES3.new(self.key, DES3.MODE_CBC, self.iv)
        return b64encode(self.iv + self.cipher.encrypt(pad(data.encode('utf-8'),
                                                      DES3.block_size)))

    def decrypt(self, data):
        raw = b64decode(data)
        self.cipher = DES3.new(self.key, DES3.MODE_CBC, raw[:DES3.block_size])
        return unpad(self.cipher.decrypt(raw[DES3.block_size:]), DES3.block_size)
