import argparse
import pathlib
import fbuilder

CIPHER_ID = {'3des': 0, 'aes-128': 1, 'aes-192': 2, 'aes-256': 3}
HMAC_ID = {'md5': 0, 'sha1': 1}


def verify(args):
    try:
        file = open(args.source_file, "r")
        file.close()
    except IOError:
        print("False")
        exit(0)

    parameters = str(args.source_file).split('_')
    hmac = HMAC_ID[parameters[0]]
    algo = CIPHER_ID[parameters[1]]

    if len(parameters) < 3:
        print("False")
        exit(0)

    hmac_algo_possib, encrypt_algo_possib, nince_possib, iv_possib, cipher_text_poss, passw = fbuilder.rebuild_file(args.source_file)
    print(
        (hmac == int(hmac_algo_possib))
        &
        (algo == int(encrypt_algo_possib))
    )


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.version = '1.0'
    parser.add_argument('-v',
                        '--version',
                        action='version')
    parser.add_argument('source_file',
                        type=pathlib.Path,
                        action='store',
                        help='file with encrypted text')

    verify(parser.parse_args())
