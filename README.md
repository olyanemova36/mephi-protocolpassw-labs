# mephi-protocolpassw-labs



## Описание
Репозиторий для курса "Протоколы с парольной защитой".

| Название лаборатной  | Статус        | Пароль    |
| -------------------  | ------------- | --------- |
| lab1_hashchecker     |       ✅      |           |
| lab2_customcipher    |       ✅      |           |
| lab3_customprotocol  |       ✅      |           |
| lab4_aggressiveikev1 |       ✅      | Qwe123    |
| lab5_mainikev1       |       ✅      | ROOT000   |
| lab6_pkcs            |       ?       |           |

