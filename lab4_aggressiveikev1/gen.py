import argparse
import hmac
from codecs import encode

STAR = "*"

N_r = "1f26436f8ae50d3aad805d26ad4d7512a7e72a81"
N_i = "f45234b575fa02bdbfd717081e92868f5e2d3773"
G_x = "2f7c491c67e3609a09c4e03342f7457ef4b8439266dba07efbd891874542de212d3cc3cc0274e680e7adee2c3476d1e61dfefa75039392e0705ae7b1e0012bdabc06cfbc43f3cd25d5ac8cb0d9ae345c61fb91f120a1d405daaeafdb275d3f33bfddd7d2d8562fa812190e977074fb177188886d6ae884ad1c0fbce3f06b4f07"
G_y = "6d026d5616c45be05e5b898411e9f95d195cea009ad22c62bef06c571b7cfbc4792f45564ec710ac584aa18d20cbc8f5f8910666b89e4ee2f95abc0230e2cba1b88ac4bba7fcc818a986c01a4ca865a5eb82884dbec85bfd7d1a303b09894dcf2e3785fd79dba225377cf8cca009ceffbb6aa38b648c4b05404f1cfaac361aff"
Ci = "e47a591f78c99d7f"
Cr = "a00b8ef0ea9ff883"
SAi = "3c00000001000000010000003001010001000000280101000080010007800e0080800200028004000280030001800b0001000c000400015180"
IDr = "0c01000000c0a80c02"


# Ni*Nr*g_x*g_y*Ci*Cr*SAi*IDr*HASH_R
def gen_output_file(passw, mode, hash_r):
    with open(passw + "_" + mode + ".txt", "w") as file:
        file.write(N_i + STAR +
                   N_r + STAR +
                   G_x + STAR +
                   G_y + STAR +
                   Ci + STAR +
                   Cr + STAR +
                   SAi + STAR +
                   IDr + STAR +
                   hash_r)


def prf(hash_type, password, nonce):
    if hash_type == 'sha1':
        return hmac.new(bytes.fromhex(password), bytes.fromhex(nonce), "sha1").digest()
    else:
        return hmac.new(bytes.fromhex(password), bytes.fromhex(nonce), "md5").digest()


def process(args):
    s_key_id = prf(args.mode, args.passw.encode().hex(), N_i + N_r).hex()
    hash_r = prf(args.mode, s_key_id, G_y + G_x + Cr + Ci + SAi + IDr).hex()
    gen_output_file(args.passw, args.mode, hash_r)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.version = '1.0'
    parser.add_argument('-v',
                        '--version',
                        action='version')
    parser.add_argument('-p',
                        '--passw',
                        action='store',
                        type=str,
                        required=True,
                        help='set the password')
    parser.add_argument('-m',
                        '--mode',
                        action='store',
                        type=str,
                        required=True,
                        help='set the hash encryption/decryption mode',
                        choices=['md5', 'sha1'])

    process(parser.parse_args())
