# IKEv1 Aggressive mode cracking

Перед работой c утилитами дополнительные библиотеки устанавливать нет необходимости.

## How to understand the utility output: 

Вся программа состоит из 2-х утилит: crack.py и gen.py. Первая соответсвенно ищет пароль перебором для предоставленного файла. Вторая
генерирует входной файл с данными для второй утилиты.

### How to use gen.py

Данная утилита генерирует файл по шаблону `[PASSWORD]_[РMAC_ALGO].txt`. Содержимое файла это данные трафика при передачи 2-х пакетов по протоколу IKEv1. Файл содержит данные куки, авторизационные данные, нонсе и хеш, который будет отпарвляться инициатору общения. Примерный шаблон внутреннего устройства файла: 
```tex
Ni*Nr*g_x*g_y*Ci*Cr*SAi*IDr*HASH_R 
```
Запуск утилиты и справка для её аргументов: 

```shell
(venv) ➜ aggressive-ikev1 python gen.py -h
usage: gen.py [-h] [-v] -p PASSW -m {md5,sha1}

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -p PASSW, --passw PASSW
                        set the password
  -m {md5,sha1}, --mode {md5,sha1}
                        set the hash encryption/decryption mode
```


Пример работы утилиты. Возьмем несколько примеров вызовов и посмотрим их 
соответствущие выводы.  

```shell
(venv) ➜ aggressive-ikev1 python gen.py -m md5 -p 000hdghsgdgjs
```

После завершения процесса создастся новый файл с данными в формате и форматом данных описанными выше. 


### How to use crack.py

Утилита принимает как параметры файл с зашифрованными данными и маску для пароля, который мы хотим получить из этих данных. Содержимое принятого утилитой фвйла это данные трафика при передачи 2-х пакетов по протоколу IKEv1. Файл должен быть составлен по определенному шаблону. Шаблон внутреннего устройства файла (из предыдущего описания утилиты): 
```tex
Ni*Nr*g_x*g_y*Ci*Cr*SAi*IDr*HASH_R 
```
Запуск утилиты и справка для её аргументов: 

```shell
(venv) ➜ aggressive-ikev1 python crack.py -h
usage: crack.py [-h] [-v] -m MASK source_file

positional arguments:
  source_file           file with encrypted information. it has specified format

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -m MASK, --mask MASK  set the mask for password enumeration 
                        a – all latin letters (u + l) + digits; 
                        d – only digits; 
                        l – lower latin letters; 
                        u – upper latin letters.
```

Пример работы утилиты. Возьмем несколько примеров вызовов и посмотрим их 
соответствущие выводы. 

```shell
(venv) ➜ python crack.py -m ddddd 12345_md5.txt
```

```shell
Password recovered! Password :  12345
```

После завершения процесса выведется найденный пароль или сообщение о том, что в маске перебора пароля не было обнаружено.
