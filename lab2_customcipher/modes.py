import algo

BLOCK_SIZE = 4


def ecb_enc(message, key, debug=False):
    counter = 0
    result = []
    block = []
    padd = padding(len(message))
    for byte in message:
        counter += 1
        block.append(byte)
        if (counter % BLOCK_SIZE != 0) & (counter == len(message)):
            text = algo.encrypt(key, block + padd, debug)
            result += text
        if counter % BLOCK_SIZE == 0:
            text = algo.encrypt(key, block, debug)
            result += text
            block = []

    return bytes(result)


def ecb_dec(message, key, debug=False):
    counter = 0
    result = []
    block = []
    message_bytes = bytes(message, 'UTF-8').strip()
    for byte in message_bytes:
        counter += 1
        block.append(byte)
        if counter % BLOCK_SIZE == 0:
            text = algo.decrypt(key, block, debug)
            result += text
            block = []

    return bytes(result)


def cbc_enc(message, iv, key, debug=False):
    counter = 0
    length = len(message)
    result = []
    block = []
    padd = padding(length)
    for byte in message:
        counter += 1
        block.append(byte)
        if (counter % BLOCK_SIZE != 0) & (counter == length):
            iv = algo.encrypt(key, algo.bxor(block + padd, iv), debug)
            result += iv
        if counter % BLOCK_SIZE == 0:
            iv = algo.encrypt(key, algo.bxor(block, iv))
            result += iv
            block = []

    return bytes(result)


def cbc_dec(message, iv, key, debug=False):
    counter = 0
    result = []
    block = []
    message_bytes = bytes(message, 'UTF-8').strip()
    for byte in message_bytes:
        counter += 1
        block.append(byte)
        if counter % BLOCK_SIZE == 0:
            plain = algo.bxor(algo.decrypt(key, block, debug), iv)
            iv = block
            result += plain
            block = []
    return bytes(result)


def padding(length):
    return [0x00 for i in range(BLOCK_SIZE - (length % BLOCK_SIZE))]
