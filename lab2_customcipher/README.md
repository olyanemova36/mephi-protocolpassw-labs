## Утилита для шифрования сообщений из файла 
Утилита реализует кастомное шифрование и дешифрование в 2-режимах ECB и CBC. Она шифрует данные из переданного файла и результат работы утилиты вывод в консоль. В утилите доступна опция вывода трейсов работы при установке соответствующего флага в консольном вызове.


### Документация к использованию 

Использование утилиты из командной строки. Вывод флажка -h. Аргументы задаются через пробел после указания флага.

```
usage: main.py [-h] [-v] [-m {cbc,ecb}] [-e] [-d] [-k KEY] [-i IV] [-g] source_file

positional arguments:
  source_file           file with message

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -m {cbc,ecb}, --mode {cbc,ecb}
                        set the encryption/decryption mode
  -e, --enc             set the encryption message process
  -d, --dec             set the decryption message process
  -k KEY, --key KEY     set the key (only 4 bytes)
  -i IV, --iv IV        set the initialization vector (only 4 bytes)
  -g, --debug           output of all intermediate measurements (defined independently)
```

### Примеры использования

#### Режим ECB (Шифрование и расшифрование)

```bash
> python main.py -m ecb -e -k keys messsage.txt 
```

```bash
> python main.py -m ecb -d -k keys messsage.txt 
```

Где, 
messsage.txt  - файл с открытым тексом для шифрования
-k keys - ключ, передеанный в командной строке в виде массива символов (строка)

Важно! Для режима -d содержимое файла, перед его преобразование, кодируется кодировкой UTF-8. 

Вывод утилиты: 
```bash
> b'\x84\xbe\xe2w\x84\xbe\xe2w'
```
```bash
> b'6/\xca\xe36/\xca\xe3'
```

#### Режим СBC (Шифрование и расшифрование)

```bash
> python main.py -m cbc -e -k keys -i ssss messsage.txt 
```

```bash
> python main.py -m cbc -d -k keys -i ssss messsage.txt 
```

Где, 
messsage.txt  - файл с открытым тексом для шифрования
-k keys - ключ, передеанный в командной строке в виде массива символов (строка)

Важно! Для режима -d содержимое файла, перед его преобразование, кодируется кодировкой UTF-8. 

Вывод утилиты: 
```bash
> b'0^\xdd\x9f \xe1Q\xb6'
```
```bash
> b'E\\\xb9\x90[J\xb9\x90'
```

#### Запуск с ошибками

1. Запуск утилиты с неправильной длиной ключа или вектора инициализации.
  ```bash
  > python main.py -m ecb -e -k keyss -g messsage.txt 
  ```

  ```bash
   Traceback (most recent call last):
   File "main.py", line 77, in <module>
     process(parser.parse_args())
   File "main.py", line 17, in process
     check_lens(args.key, args.iv)
   File "main.py", line 12, in check_lens
     raise Exception('Incorrect parameters length!')
   Exception: Incorrect parameters length!

  ```

2. Запуск утилиты без параметра-файла с исходным текстом.

  ```bash
  > python main.py -m ecb -e -k keyss -g
  ```

  ```bash
  usage: main.py [-h] [-v] [-m {cbc,ecb}] [-e] [-d] [-k KEY] [-i IV] [-g]     
  source_file 
  main.py: error: the following arguments are required: source_file
  ```

#### Запуск утилиты с выводом логов
  ```bash
  python main.py -m ecb -e -k keys -g messsage.txt  
  ```

  ```bash
  Round 0: key_0 :  b'keys' text :  b'mess'
  Round 1: key_1 :  b'syek' text :  b'occg'
  Round 2: key_2 :  b'\x18\x1c\x1c\x18' text :  b'\x9c\xa2\xfeo'
  Round 0: key_0 :  b'keys' text :  b'mess'
  Round 1: key_1 :  b'syek' text :  b'occg'
  Round 2: key_2 :  b'\x18\x1c\x1c\x18' text :  b'\x9c\xa2\xfeo'
  b'\x84\xbe\xe2w\x84\xbe\xe2w'

  ```

### Добавление данных в файл

Данные из файла, который передается в качестве параметра утилите внутри программы кодируются в байты, перед тем как будут декодированы из UTF-8. Чтобы получалось правильное расшифрование и шифрование следует проверять данные в файле.
