import modes
import key_generator
import argparse
import pathlib

BYTES_LOWER = {'0': 0x00, '1': 0x01, '2': 0x02, '3': 0x03, '4': 0x04, '5': 0x05, '6': 0x06, '7': 0x07, '8': 0x08, '9': 0x09,
         'a': 0x0a, 'b': 0x0b, 'c': 0x0c, 'd': 0x0d, 'e': 0x0e, 'f': 0x0f,
         'A': 0x0a, 'B': 0x0b, 'C': 0x0c, 'D': 0x0d, 'E': 0x0e, 'F': 0x0f}

BYTES_HIGHER = {'0': 0x00, '1': 0x10, '2': 0x20, '3': 0x30, '4': 0x40, '5': 0x50, '6': 0x60, '7': 0x70, '8': 0x80, '9': 0x90,
         'a': 0xa0, 'b': 0xb0, 'c': 0xc0, 'd': 0xd0, 'e': 0xe0, 'f': 0xf0,
         'A': 0xa0, 'B': 0xb0, 'C': 0xc0, 'D': 0xd0, 'E': 0xe0, 'F': 0xf0}


def check_lens(key, iv=None):
    if iv is not None:
        if len(key) != (modes.BLOCK_SIZE * 2) & len(iv) != modes.BLOCK_SIZE:
            raise Exception('Incorrect parameters length!')
    else:
        if len(key) != (modes.BLOCK_SIZE * 2):
            raise Exception('Incorrect parameters length!')


def get_bytes_from_str(string):
    res = []
    length = len(string)
    for i in range(0, length-1, 2):
        if i % 2 == 0:
            res.append(BYTES_HIGHER[string[i]] + BYTES_LOWER[string[i+1]])
    return bytes(res)


def get_plaintext(string):
    try:
        with open(string, "r") as file:
            print("File has opened for reading.")
            return file.read()
    except IOError:
        print("File has opened already.")


def process(args):
    if args.key is not None or args.iv is not None:
        check_lens(args.key, args.iv)
        key = key_generator.Key(args.key)
        message = get_bytes_from_str(get_plaintext(args.source_file))
        if len(message) == 0:
            raise Exception('Empty file for encoding!')
        if args.enc:
            if args.mode == 'ecb':
                print(modes.ecb_enc(message, key, args.debug))
            else:
                iv = get_bytes_from_str(args.iv)
                print(modes.cbc_enc(message, iv, key, args.debug))
        elif args.dec:
            if args.mode == 'ecb':
                print(modes.ecb_dec(message, key, args.debug))
            else:
                iv = bytes(args.iv, 'UTF-8').strip()
                print(modes.cbc_dec(message, iv, key, args.debug))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.version = '1.0'
    parser.add_argument('-v',
                        '--version',
                        action='version')
    parser.add_argument('-m',
                        '--mode',
                        action='store', type=str,
                        help='set the encryption/decryption mode ',
                        choices=['cbc', 'ecb'])
    parser.add_argument('-e', '--enc',
                        action='store_true',
                        help='set the encryption message process')
    parser.add_argument('-d', '--dec',
                        action='store_true',
                        help='set the decryption message process')
    parser.add_argument('-k', '--key',
                        action='store',
                        type=str,
                        help='set the key (only 8 hex digits)')
    parser.add_argument('-i', '--iv',
                        action='store',
                        type=str,
                        help='set the initialization vector (only 8 hex digits)')
    parser.add_argument('-g', '--debug',
                        action='store_true',
                        help='output of all intermediate measurements (defined independently)')
    parser.add_argument('source_file',
                        type=pathlib.Path,
                        help='file with message')
    process(parser.parse_args())


