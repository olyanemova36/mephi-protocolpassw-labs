BYTES_LOWER = {'0': 0x00, '1': 0x01, '2': 0x02, '3': 0x03, '4': 0x04, '5': 0x05, '6': 0x06, '7': 0x07, '8': 0x08, '9': 0x09,
         'a': 0x0a, 'b': 0x0b, 'c': 0x0c, 'd': 0x0d, 'e': 0x0e, 'f': 0x0f,
         'A': 0x0a, 'B': 0x0b, 'C': 0x0c, 'D': 0x0d, 'E': 0x0e, 'F': 0x0f}

BYTES_HIGHER = {'0': 0x00, '1': 0x10, '2': 0x20, '3': 0x30, '4': 0x40, '5': 0x50, '6': 0x60, '7': 0x70, '8': 0x80, '9': 0x90,
         'a': 0xa0, 'b': 0xb0, 'c': 0xc0, 'd': 0xd0, 'e': 0xe0, 'f': 0xf0,
         'A': 0xa0, 'B': 0xb0, 'C': 0xc0, 'D': 0xd0, 'E': 0xe0, 'F': 0xf0}


def bit_reverse(byte_array, n):
    res = []
    for byte in byte_array:
       res.append(int(format(byte, '0%db' % n)[::-1], 2))
    return res


def get_bytes_from_str(string):
    res = []
    length = len(string)
    for i in range(0, length-1, 2):
        if i % 2 == 0:
            res.append(BYTES_HIGHER[string[i]] + BYTES_LOWER[string[i+1]])
    return bytes(res)


class Key:
    def __init__(self, key):
        self.key_str = key

    def get_key(self, round):
        if round == 0:
            return get_bytes_from_str(self.key_str)
        elif round == 1:
            return self.__invert()
        else:
            return self.bxor()

    def __invert(self):
        return bit_reverse(get_bytes_from_str(self.key_str), 8)

    def bxor(self):
        one = get_bytes_from_str(self.key_str)
        two = self.__invert()
        """ XOR two byte strings """
        return bytes([_a ^ _b for _a, _b in zip(one, two)])
