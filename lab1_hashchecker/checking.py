import os
import sys

ENCODINGS = ['UTF-8', 'UTF-16-BE', 'UTF-16-LE', 'ASCII']
HASH_ALGOS = ['MD4', 'MD5', 'SHA1', 'SHA256', 'SHA512']

ARG_COUNT_GEN = 5
ARG_COUNT_CRACK = 4


def check_argv_gen():

    if len(sys.argv[1:]) < ARG_COUNT_GEN | len(sys.argv[1:]) > ARG_COUNT_GEN:
        print("Wrong count of arguments!")

    if not os.path.exists(sys.argv[1]):
        raise FileExistsError("File with passwords wasn't find!")

    try:
        count = int(sys.argv[4])
    except:
        raise ValueError

    if sys.argv[2] not in ENCODINGS:
        raise ValueError("The program doesn't support such type of encoding - {}", sys.argv[2])

    if sys.argv[3] not in HASH_ALGOS:
        raise ValueError("The program doesn't support such type of hash algorithm - {}", sys.argv[3])


def check_argv_crack():
    if len(sys.argv[1:]) < ARG_COUNT_CRACK | len(sys.argv[1:]) > ARG_COUNT_CRACK:
        print("Wrong count of arguments!")

    if not os.path.exists(sys.argv[1]):
        raise FileExistsError("File with passwords wasn't find!")

    if not os.path.exists(sys.argv[4]):
        raise FileExistsError("File with hashes wasn't find!")

    if sys.argv[2] not in ENCODINGS:
        raise ValueError("The program doesn't support such type of encoding - {}", sys.argv[2])

    if sys.argv[3] not in HASH_ALGOS:
        raise ValueError("The program doesn't support such type of hash algorithm - {}", sys.argv[3])