import random
import sys
import time

import cracking
import generate
import hash_random

MAX_COUNT = 500


def banchmarking(tests_count, encoding, algo):
    out_filename = "test_out.txt"

    tests_count = int(tests_count)

    with open('benchmark.txt', 'w') as f:
        for test in range(tests_count):
            pass_count = 5640
            file = gen_passwords(pass_count)
            count = random.randint(MAX_COUNT, 4 * MAX_COUNT)
            begin = time.time()
            generate.generate(file, encoding, algo, count, out_filename)
            cracking.crack(file, encoding, algo, out_filename)
            end = time.time()
            f.write('passwords: ' + str(pass_count) + ' hashes: ' + str(count)
                    + ' speed: ' + str(pass_count / (end - begin)) + ' cand/sc\n')


def gen_passwords(passwords_count):
    file_name = 'test.txt'
    with open('test.txt', 'w') as f:
        for i in range(passwords_count):
            f.write(hash_random.generate_random_str() + '\n')
    return file_name


if __name__ == '__main__':
    banchmarking(sys.argv[1], sys.argv[2], sys.argv[3])