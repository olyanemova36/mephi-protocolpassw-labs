# IKEv1 Main mode cracking

Перед работой нужно настроить крипптографические библиотеки для разрешения коллизий между одинаковыми названиями пакетов для использования модуля с реализациями алгоритмов шифрования.

## How to understand the utility output: 

Вся программа состоит из 2-х утилит: crack.py и gen.py. Первая соответсвенно ищет пароль перебором для предоставленного файла. Вторая
генерирует входной файл с данными для второй утилиты.

### How to use gen.py

Данная утилита генерирует файл по шаблону `[PASSWORD]_[РMAC_ALGO]_[ENC_ALGO].txt`. Содержимое файла это данные трафика при передачи 5-ти пакетов по протоколу IKEv1 в Main mode. Файл содержит данные куки, авторизационные данные, нонсе, зашифрованные хеш и айди, которые будут отправляться утилите crack.py. Примерный шаблон внутреннего устройства файла: 
```tex
HASH_id*ALG_id*Ni*Nr*g_x*g_y*g_xy*Ci*Cr*SAi*E_k
```
Запуск утилиты и справка для её аргументов: 

```shell
(venv) ➜  main-ikev1 python gen.py -h 
usage: gen.py [-h] [-v] -p PASSW -m {md5,sha1} -a {3des,aes128,aes192,aes256}

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -p PASSW, --passw PASSW
                        set the password
  -m {md5,sha1}, --mode {md5,sha1}
                        set the hash encryption mode
  -a {3des,aes128,aes192,aes256}, --algo {3des,aes128,aes192,aes256}
                        set the algo encryption mode
```


Пример работы утилиты. Возьмем несколько примеров вызовов и посмотрим их 
соответствущие выводы.  

```shell
(venv) ➜ aggressive-ikev1 python gen.py -p qwerty001 -m md5 -a aes256
```

После завершения процесса создастся новый файл с данными в формате и форматом данных описанными выше. 


### How to use crack.py

Утилита принимает как параметры файл с зашифрованными данными и маску для пароля, который мы хотим получить из этих данных. Содержимое принятого утилитой файла это данные трафика при передачи 5-ти пакетов по протоколу IKEv1. Файл должен быть составлен по определенному шаблону. Утилита расшифровывает записанные в файл данные и генерирует проверочных хеш. Перебирая пароль, сравнивает хеши и устанавливает пароль, на котором шифровались данные. Шаблон внутреннего устройства файла (из предыдущего описания утилиты): 
```tex
HASH_id*ALG_id*Ni*Nr*g_x*g_y*g_xy*Ci*Cr*SAi*E_k
```
Запуск утилиты и справка для её аргументов: 

```shell
(venv) ➜  main-ikev1 python crack.py -h
usage: crack.py [-h] [-v] [-d DICT] -m MASK source_file

positional arguments:
  source_file           file with encrypted information. it has specified format

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -d DICT, --dict DICT  file with dictionary enumeration. it has specified format
  -m MASK, --mask MASK  set the mask for password enumeration 
                        a – all latin letters (u + l) + digits; 
                        d – only digits; 
                        l – lower latin letters; 
                        u – upper latin letters.
```

Пример работы утилиты. Возьмем несколько примеров вызовов и посмотрим их 
соответствущие выводы. 

```shell
(venv) ➜ python crack.py -d dict.txt -m ddd qwerty001_md5_aes256.txt 
```

```shell
Password recovered! Password :  qwerty001
```

После завершения процесса выведется найденный пароль или сообщение о том, что в маске перебора пароля не было обнаружено.
