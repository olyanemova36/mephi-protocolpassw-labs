import argparse
import pathlib
import string
import gen
from Crypto.Cipher import DES3
from Crypto.Cipher import AES

MD5_HASH_LEN = 16
SHA1_HASH_LEN = 20
BOOST = 2
IDii_SIZE = 24

AVAILABLE_MASKS = ['a', 'd', 'l', 'u']
IDii_pref = "0800000c"
IDii_payload_size = 4
HASH_I_payload_size = 4


def get_hash_algo(id):
    if id == '1':
        return 'md5'
    elif id == '2':
        return 'sha1'
    else:
        print("Incorrect Hash mode in file.")
        exit(1)


def get_hashmac_output_size(hash):
    if hash == 'md5':
        return MD5_HASH_LEN*2
    else:
        return SHA1_HASH_LEN*2


def get_enc_algo(id):
    if id == '5':
        return '3des'
    elif id == '7':
        return 'aes128'
    elif id == '8':
        return 'aes192'
    elif id == '9':
        return 'aes256'
    else:
        print("Incorrect Encryption mode in file.")
        exit(1)


# HASH_id*ALG_id*Ni*Nr*g_x*g_y*g_xy*Ci*Cr*SAi*E_k
def rebuild_file(file):
    with open(file, "r") as f:
        text = f.read()
        text_chunks = text.split('*')
        if len(text_chunks) == 11:
            hash = get_hash_algo(text_chunks[0])
            algo = get_enc_algo(text_chunks[1])
            N_i = text_chunks[2]
            N_r = text_chunks[3]
            G_x = text_chunks[4]
            G_y = text_chunks[5]
            G_xy = text_chunks[6]
            Ci = text_chunks[7]
            Cr = text_chunks[8]
            SAi = text_chunks[9]
            E_k = text_chunks[10]

            N_THIRD = G_xy + Ci + Cr
            N_SECOND_without_IDii_b = G_x + G_y + Ci + Cr + SAi
            N_FIRST = N_i + N_r

            iv = gen.get_iv_for_mode(algo, hash, G_x + G_y)

            return hash, algo, N_FIRST, N_SECOND_without_IDii_b, N_THIRD, iv, E_k
        else:
            print("Incorrect format file for application.")
            exit(0)


def alphabet(letter):
    if letter == 'a':
        return list(string.ascii_letters) + list(string.digits + '!')
    if letter == 'l':
        return list(string.ascii_lowercase)
    if letter == 'u':
        return list(string.ascii_uppercase)
    if letter == 'd':
        return list(string.digits + '!')


def check_mask(mask):
    result = True
    for mask_parameter in mask:
        result &= mask_parameter in AVAILABLE_MASKS
    return result


def gen_passwords(mask, dict_file):
    aphs = [alphabet(letter) for letter in mask]
    with open(dict_file, "r") as f:
        res = f.readlines()
    words = []
    for word in res:
        if word[-1] == '\n':
            words.append(word[:-1])
        else:
            words.append(word)
    return [words] + aphs


def get_encoder(algo, iv, key):
    if algo == '3des':
        cipher = DES3.new(key, DES3.MODE_CBC, IV=iv)
        return cipher
    if algo == 'aes128':
        cipher = AES.new(key, AES.MODE_CBC, IV=iv)
        return cipher
    if algo == 'aes192':
        cipher = AES.new(key, AES.MODE_CBC, IV=iv)
        return cipher
    else:
        cipher = AES.new(key, AES.MODE_CBC, IV=iv)
        return cipher


def ords(alphabets):
    passwords_count = 1
    ords_bases = []

    for custom_alphabet in alphabets:
        passwords_count *= len(custom_alphabet)

    for i in range(len(alphabets)):
        if i == 0:
            ords_bases.append(len(alphabets[i]))
        else:
            ords_bases.append(ords_bases[i-1] * len(alphabets[i]))

    return ords_bases, passwords_count


# nonce_1 -> N_i + N_r
# nonce_2_prt -> G_x + G_y + Ci + Cr + SAi
# nonce_3 -> G_xy + Ci + Cr
def process(args):
    if check_mask(args.mask):
        passwords = gen_passwords(args.mask, args.dict)
        hash, algo, nonce_1, nonce_2_prt, nonce_3, iv, e_k = rebuild_file(args.source_file)
        brute(passwords, hash, algo, nonce_1, nonce_2_prt, nonce_3, e_k, iv)
    else:
        print("Incorrect mask parameters!")
        exit(0)


def decrypt(hash, algo, e_k, iv, s_key_id_e):
    text = get_encoder(algo, iv, s_key_id_e).decrypt(bytes.fromhex(e_k)).hex()

    sep = get_hashmac_output_size(hash)
    return text[:IDii_SIZE], text[(IDii_SIZE + (HASH_I_payload_size*2)):(IDii_SIZE + (HASH_I_payload_size*2) + sep)]


def brute(passwords_list, hash, algo, nonce_1, nonce_2_prt, nonce_3, e_k, iv):
    bases, passwords = ords(passwords_list)
    for password in range(passwords):
        password_word = ''
        for i in range(len(bases)):
            mask_alphabet = passwords_list[i]
            len_alphabet = len(mask_alphabet)
            if i == 0:
                password_word += mask_alphabet[password % bases[i]]
            else:
                password_word += mask_alphabet[((password * len_alphabet) // bases[i]) % len_alphabet]
        s_key_id = gen.prf(hash, password_word.encode().hex(), nonce_1).hex()
        s_key_id_e = gen.key_gen(hash, algo, s_key_id, nonce_3)
        ID, hash_i = decrypt(hash, algo, e_k, iv, s_key_id_e)
        hash_exp = gen.prf(hash, s_key_id, nonce_2_prt + ID[(IDii_payload_size*2):]).hex()
        if hash_i == hash_exp:
            print('Password recovered! Password : ', password_word)
            return
    print('The password was not found during the enumeration.')


def ikev(mode, password, nonse_1, nonse_2):
    s_key_id = gen.prf(mode, password, nonse_1).hex()
    return gen.prf(mode, s_key_id, nonse_2).hex()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.version = '1.0'
    parser.add_argument('-v',
                        '--version',
                        action='version')
    parser.add_argument('-d',
                        '--dict',
                        type=pathlib.Path,
                        action='store',
                        help='file with dictionary enumeration. it has specified format')
    parser.add_argument('-m',
                        '--mask',
                        action='store',
                        type=str,
                        required=True,
                        help=
                        '''set the mask for password enumeration 
                            a – all latin letters (u + l) + digits; 
                            d – only digits; 
                            l – lower latin letters; 
                            u – upper latin letters. 
                        ''')
    parser.add_argument('source_file',
                        type=pathlib.Path,
                        action='store',
                        help='file with encrypted information. it has specified format')

    process(parser.parse_args())
